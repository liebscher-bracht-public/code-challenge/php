# Php

Simple php envirnoment

## Getting started

Install Docker Desktop

- https://docs.docker.com/desktop/mac/install/
- https://docs.docker.com/desktop/windows/install/

## Usage

To start Docker environment, go to terminal in project folder.

### Start

    docker-compose up -d

### Stop 
    
    docker-compose down

## Browser

    http://localhost

## Database usage

    root:root@127.0.0.1:3306
    
    dbuser:dbpassword@127.0.0.1:3306

## Mailhog

    http://localhost:8025